package com.ethersofts.declarations.components.saved;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ethersofts.declarations.R;
import com.ethersofts.declarations.models.DeclarationModel;
import com.ethersofts.declarations.services.api.nazk.DeclarationDto;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.OrderedRealmCollection;
import io.realm.RealmRecyclerViewAdapter;

public class DeclarationAdapter extends RealmRecyclerViewAdapter<DeclarationModel, DeclarationAdapter.ViewHolder> {

    private IOnItemClickListener mPdfClickListener;

    private IOnItemClickListener mFavouriteClickListener;

    private IOnItemClickListener mEditCommentClickListener;

    DeclarationAdapter(@Nullable OrderedRealmCollection<DeclarationModel> data, boolean autoUpdate) {
        super(data, autoUpdate);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_declaration_saved, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final DeclarationModel item = getData().get(position);

        holder.mTvFirstName.setText(item.getFirstName());
        holder.mTvLastName.setText(item.getLastName());
        holder.mTvWorkPlace.setText(item.getWorkPlace());
        holder.mTvComment.setText(item.getComment());

        holder.mBtnPdf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mPdfClickListener != null) {
                    mPdfClickListener.onClick(item);
                }
            }
        });

        holder.mBtnFavourite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mFavouriteClickListener != null) {
                    mFavouriteClickListener.onClick(item);
                }
            }
        });

        holder.mBtnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mEditCommentClickListener != null) {
                    mEditCommentClickListener.onClick(item);
                }
            }
        });
    }

    public void setPdfClickListener(IOnItemClickListener pdfClickListener) {
        mPdfClickListener = pdfClickListener;
    }

    public void setFavouriteClickListener(IOnItemClickListener favouriteClickListener) {
        mFavouriteClickListener = favouriteClickListener;
    }

    public void setEditCommentClickListener(IOnItemClickListener editCommentClickListener) {
        mEditCommentClickListener = editCommentClickListener;
    }

    public interface IOnItemClickListener {
        void onClick(DeclarationModel item);
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_first_name)
        TextView mTvFirstName;

        @BindView(R.id.tv_last_name)
        TextView mTvLastName;

        @BindView(R.id.tv_workplace)
        TextView mTvWorkPlace;

        @BindView(R.id.tv_comment)
        TextView mTvComment;

        @BindView(R.id.btn_favourite)
        View mBtnFavourite;

        @BindView(R.id.btn_pdf)
        View mBtnPdf;

        @BindView(R.id.btn_edit)
        View mBtnEdit;

        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
