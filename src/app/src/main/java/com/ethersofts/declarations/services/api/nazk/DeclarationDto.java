package com.ethersofts.declarations.services.api.nazk;

import com.google.gson.annotations.SerializedName;

public class DeclarationDto {

    @SerializedName("id")
    public String Id;

    @SerializedName("firstname")
    public String FirstName;

    @SerializedName("lastname")
    public String LastName;

    @SerializedName("placeOfWork")
    public String WorkPlace;

    @SerializedName("linkPDF")
    public String PdfUrl;
}
