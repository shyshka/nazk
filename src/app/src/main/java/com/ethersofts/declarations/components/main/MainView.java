package com.ethersofts.declarations.components.main;

import com.ethersofts.declarations.services.api.nazk.DeclarationsDto;
import com.hannesdorfmann.mosby3.mvp.MvpView;

interface MainView extends MvpView {
    void showData(DeclarationsDto data);

    void showError(String message);

    void hideLoading();

    void showLoading();
}
