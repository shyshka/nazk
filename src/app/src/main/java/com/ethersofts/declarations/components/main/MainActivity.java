package com.ethersofts.declarations.components.main;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.SearchView;
import android.widget.Toast;

import com.ethersofts.declarations.R;
import com.ethersofts.declarations.components.dialogs.EditCommentDialog;
import com.ethersofts.declarations.components.pdf.PdfActivity;
import com.ethersofts.declarations.components.saved.SavedActivity;
import com.ethersofts.declarations.services.api.nazk.DeclarationDto;
import com.ethersofts.declarations.services.api.nazk.DeclarationsDto;
import com.hannesdorfmann.mosby3.mvp.MvpActivity;

import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends MvpActivity<MainView, MainPresenter>
        implements MainView {

    private DeclarationAdapter mDeclarationAdapter;

    @BindView(R.id.search_view)
    SearchView mSearchView;

    @BindView(R.id.swipe)
    SwipeRefreshLayout mSwipeRefresh;

    @BindView(R.id.recycler_view)
    RecyclerView mRecyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        initUI();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuItem savedItem = menu.add("Saved");
        savedItem.setIcon(R.drawable.ic_star_full);
        savedItem.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        savedItem.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                startActivity(new Intent(MainActivity.this, SavedActivity.class));
                return false;
            }
        });

        return super.onCreateOptionsMenu(menu);
    }

    private void initUI() {
        mSwipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                presenter.loadDataAsync(mSearchView.getQuery().toString());
            }
        });

        mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                presenter.loadDataAsync(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        mDeclarationAdapter = new DeclarationAdapter();
        mDeclarationAdapter.setPdfClickListener(new DeclarationAdapter.IOnItemClickListener() {
            @Override
            public void onClick(DeclarationDto item) {
                startActivity(new Intent(MainActivity.this, PdfActivity.class)
                        .putExtra(PdfActivity.EXTRA_URL, item.PdfUrl));
            }
        });
        mDeclarationAdapter.setFavouriteClickListener(new DeclarationAdapter.IOnItemClickListener() {
            @Override
            public void onClick(final DeclarationDto item) {
                EditCommentDialog dialog = new EditCommentDialog(MainActivity.this);
                dialog.setSaveListener(new EditCommentDialog.IOnSaveListener() {
                    @Override
                    public void onSave(String comment) {
                        presenter.addDeclaration(item, comment);
                        Toast.makeText(MainActivity.this, String.format(Locale.getDefault(), "Декларація %s %s збережена", item.LastName, item.FirstName), Toast.LENGTH_SHORT).show();
                        mDeclarationAdapter.refresh(item);
                    }
                });
                dialog.edit(null);
            }
        });
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.setAdapter(mDeclarationAdapter);
    }

    @NonNull
    @Override
    public MainPresenter createPresenter() {
        return new MainPresenter();
    }

    @Override
    public void showData(DeclarationsDto data) {
        mDeclarationAdapter.setItems(data.Items);
    }

    @Override
    public void showError(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void hideLoading() {
        mSwipeRefresh.setRefreshing(false);
    }

    @Override
    public void showLoading() {
        mSwipeRefresh.setRefreshing(true);
    }
}
