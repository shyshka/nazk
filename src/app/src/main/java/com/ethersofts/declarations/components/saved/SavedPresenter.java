package com.ethersofts.declarations.components.saved;

import android.support.annotation.NonNull;

import com.ethersofts.declarations.models.DeclarationModel;
import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;

import io.realm.Realm;

class SavedPresenter extends MvpBasePresenter<SavedView> {

    private Realm mRealm;

    SavedPresenter() {
        mRealm = Realm.getDefaultInstance();
    }

    public void loadDataAsync() {
        if (isViewAttached()) {
            getView().showData(mRealm.where(DeclarationModel.class)
                    .findAll());
        }
    }

    public void deleteItem(final DeclarationModel item) {
        mRealm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(@NonNull Realm realm) {
                mRealm.where(DeclarationModel.class)
                        .equalTo(DeclarationModel.FIELD_ID, item.getId())
                        .findAll()
                        .deleteAllFromRealm();
            }
        });
    }

    public void editComment(final DeclarationModel item, final String value) {
        mRealm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(@NonNull Realm realm) {
                DeclarationModel localItem = mRealm.where(DeclarationModel.class)
                        .equalTo(DeclarationModel.FIELD_ID, item.getId())
                        .findFirst();
                localItem.setComment(value);
            }
        });
    }
}
