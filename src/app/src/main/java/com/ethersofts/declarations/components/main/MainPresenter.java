package com.ethersofts.declarations.components.main;

import android.support.annotation.NonNull;

import com.ethersofts.declarations.models.DeclarationModel;
import com.ethersofts.declarations.services.api.nazk.DeclarationDto;
import com.ethersofts.declarations.services.api.nazk.DeclarationsDto;
import com.ethersofts.declarations.services.api.nazk.InazkApi;
import com.ethersofts.declarations.services.api.nazk.NazkApiProvider;
import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;

import io.realm.Realm;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

class MainPresenter extends MvpBasePresenter<MainView> {

    private final Realm mRealm;
    private InazkApi mNazkApi;

    MainPresenter() {
        mNazkApi = NazkApiProvider.getApi();
        mRealm = Realm.getDefaultInstance();
    }

    public void loadDataAsync(String query) {
        getView().showLoading();

        mNazkApi.getDeclarations(query).enqueue(new Callback<DeclarationsDto>() {
            @Override
            public void onResponse(@NonNull Call<DeclarationsDto> call, @NonNull Response<DeclarationsDto> response) {
                if (isViewAttached()) {
                    getView().hideLoading();
                    getView().showData(response.body());
                }
            }

            @Override
            public void onFailure(@NonNull Call<DeclarationsDto> call, @NonNull Throwable t) {
                if (isViewAttached()) {
                    getView().hideLoading();
                    getView().showError(t.getMessage());
                }
            }
        });
    }

    public void addDeclaration(final DeclarationDto item, final String comment) {
        mRealm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(@NonNull Realm realm) {
                realm.insertOrUpdate(new DeclarationModel(item.Id,
                        item.LastName, item.FirstName, item.WorkPlace, item.PdfUrl, comment));
            }
        });
    }
}
