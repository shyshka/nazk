package com.ethersofts.declarations.services.api.nazk;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class DeclarationsDto {

    @SerializedName("items")
    public List<DeclarationDto> Items = new ArrayList<>();

}
