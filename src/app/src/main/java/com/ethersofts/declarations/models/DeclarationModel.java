package com.ethersofts.declarations.models;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class DeclarationModel extends RealmObject {

    public static final String FIELD_ID = "mId";
    @PrimaryKey
    private String mId;

    private String mLastName;

    private String mFirstName;

    private String mComment;

    private String mPdfUrl;

    private String mWorkPlace;

    public DeclarationModel() {
    }

    public DeclarationModel(String id, String lastName, String firstName, String workPlace, String pdfUrl, String comment) {
        mId = id;
        mLastName = lastName;
        mFirstName = firstName;
        mComment = comment;
        mPdfUrl = pdfUrl;
        mWorkPlace = workPlace;
    }

    public String getLastName() {
        return mLastName;
    }

    public void setLastName(String lastName) {
        mLastName = lastName;
    }

    public String getFirstName() {
        return mFirstName;
    }

    public void setFirstName(String firstName) {
        mFirstName = firstName;
    }

    public String getComment() {
        return mComment;
    }

    public void setComment(String comment) {
        mComment = comment;
    }

    public String getPdfUrl() {
        return mPdfUrl;
    }

    public void setPdfUrl(String pdfUrl) {
        mPdfUrl = pdfUrl;
    }

    public String getWorkPlace() {
        return mWorkPlace;
    }

    public void setWorkPlace(String workPlace) {
        mWorkPlace = workPlace;
    }

    public String getId() {
        return mId;
    }
}
