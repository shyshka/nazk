package com.ethersofts.declarations.services.api.nazk;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface InazkApi {

    @GET("declaration/")
    Call<DeclarationsDto> getDeclarations(@Query("q") String query);
}
