package com.ethersofts.declarations.components.saved;

import com.ethersofts.declarations.models.DeclarationModel;
import com.hannesdorfmann.mosby3.mvp.MvpView;

import io.realm.OrderedRealmCollection;
import io.realm.RealmResults;

interface SavedView extends MvpView {
    void showData(OrderedRealmCollection<DeclarationModel> data);
}
