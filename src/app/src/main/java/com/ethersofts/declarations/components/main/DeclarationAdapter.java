package com.ethersofts.declarations.components.main;

import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ethersofts.declarations.R;
import com.ethersofts.declarations.models.DeclarationModel;
import com.ethersofts.declarations.services.api.nazk.DeclarationDto;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.Realm;

public class DeclarationAdapter extends RecyclerView.Adapter<DeclarationAdapter.ViewHolder> {

    private List<DeclarationDto> mItems = new ArrayList<>();

    private IOnItemClickListener mPdfClickListener;

    private IOnItemClickListener mFavouriteClickListener;

    private Realm mRealm;

    DeclarationAdapter() {
        mRealm = Realm.getDefaultInstance();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_declaration, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final DeclarationDto item = mItems.get(position);

        holder.mTvFirstName.setText(item.FirstName);
        holder.mTvLastName.setText(item.LastName);
        holder.mTvWorkPlace.setText(item.WorkPlace);

        holder.mBtnPdf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mPdfClickListener != null) {
                    mPdfClickListener.onClick(item);
                }
            }
        });

        holder.mBtnFavourite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mFavouriteClickListener != null) {
                    mFavouriteClickListener.onClick(item);
                }
            }
        });

        if (mRealm.where(DeclarationModel.class)
                .equalTo(DeclarationModel.FIELD_ID, item.Id)
                .findFirst() != null) {
            holder.mBtnFavourite.setImageResource(R.drawable.ic_star_full);
        } else {
            holder.mBtnFavourite.setImageResource(R.drawable.ic_star_border);
        }

    }

    public void setItems(List<DeclarationDto> items) {
        mItems.clear();
        mItems.addAll(items);
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    public void setPdfClickListener(IOnItemClickListener pdfClickListener) {
        mPdfClickListener = pdfClickListener;
    }

    public void setFavouriteClickListener(IOnItemClickListener favouriteClickListener) {
        mFavouriteClickListener = favouriteClickListener;
    }

    public void refresh(DeclarationDto item) {
        notifyItemChanged(mItems.indexOf(item));
    }

    public interface IOnItemClickListener {
        void onClick(DeclarationDto item);
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_first_name)
        TextView mTvFirstName;

        @BindView(R.id.tv_last_name)
        TextView mTvLastName;

        @BindView(R.id.tv_workplace)
        TextView mTvWorkPlace;

        @BindView(R.id.btn_favourite)
        FloatingActionButton mBtnFavourite;

        @BindView(R.id.btn_pdf)
        View mBtnPdf;

        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
