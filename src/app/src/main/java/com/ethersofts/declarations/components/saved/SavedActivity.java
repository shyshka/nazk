package com.ethersofts.declarations.components.saved;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.ethersofts.declarations.R;
import com.ethersofts.declarations.components.dialogs.EditCommentDialog;
import com.ethersofts.declarations.components.pdf.PdfActivity;
import com.ethersofts.declarations.models.DeclarationModel;
import com.hannesdorfmann.mosby3.mvp.MvpActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.OrderedRealmCollection;

public class SavedActivity extends MvpActivity<SavedView, SavedPresenter>
        implements SavedView {

    private DeclarationAdapter mAdapter;

    @BindView(R.id.recycler_view)
    RecyclerView mRecyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_saved);
        ButterKnife.bind(this);

        presenter.loadDataAsync();
    }

    @NonNull
    @Override
    public SavedPresenter createPresenter() {
        return new SavedPresenter();
    }

    @Override
    public void showData(OrderedRealmCollection<DeclarationModel> data) {
        mAdapter = new DeclarationAdapter(data, true);
        mAdapter.setPdfClickListener(new DeclarationAdapter.IOnItemClickListener() {
            @Override
            public void onClick(DeclarationModel item) {
                startActivity(new Intent(SavedActivity.this, PdfActivity.class)
                        .putExtra(PdfActivity.EXTRA_URL, item.getPdfUrl()));
            }
        });
        mAdapter.setFavouriteClickListener(new DeclarationAdapter.IOnItemClickListener() {
            @Override
            public void onClick(DeclarationModel item) {
                presenter.deleteItem(item);
            }
        });
        mAdapter.setEditCommentClickListener(new DeclarationAdapter.IOnItemClickListener() {
            @Override
            public void onClick(final DeclarationModel item) {
                EditCommentDialog dialog = new EditCommentDialog(SavedActivity.this);
                dialog.setSaveListener(new EditCommentDialog.IOnSaveListener() {
                    @Override
                    public void onSave(String value) {
                        presenter.editComment(item, value);
                    }
                });
                dialog.edit(item.getComment());
            }
        });
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
    }
}
