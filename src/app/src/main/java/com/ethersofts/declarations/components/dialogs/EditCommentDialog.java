package com.ethersofts.declarations.components.dialogs;

import android.content.Context;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import com.ethersofts.declarations.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class EditCommentDialog {

    private AlertDialog mDialog;

    private IOnSaveListener mSaveListener;

    @BindView(R.id.et_comment)
    EditText mEtComment;

    public EditCommentDialog(Context context) {

        View view = LayoutInflater.from(context).inflate(R.layout.dlg_edit_comment, null);
        ButterKnife.bind(this, view);

        mDialog = new AlertDialog.Builder(context)
                .setView(view)
                .create();
    }

    @OnClick(R.id.btn_save)
    void onBtnSaveClicked() {
        if (mSaveListener != null) {
            mSaveListener.onSave(mEtComment.getText().toString());
        }
        mDialog.dismiss();
    }

    public void edit(String value) {
        mEtComment.setText(value);
        mDialog.show();
    }

    public void setSaveListener(IOnSaveListener saveListener) {
        mSaveListener = saveListener;
    }

    public interface IOnSaveListener {
        void onSave(String value);
    }
}
